## LAW
- https://github.com/riga/law/tree/master
- https://www.youtube.com/watch?v=OC9lilstpMs

- presentation
    - https://indico.cern.ch/event/773049/contributions/3473247/attachments/1938294/3212829/2019-11-05_Rieger_law.pdf
    - https://indico.cern.ch/event/1097499/contributions/4617472/attachments/2348703/4005689/2021-11-18_law.pdf
    - https://github.com/riga/law_pyhep2020
- example
    - https://github.com/riga/law/tree/master/examples/htcondor_at_cern
        1. A task can access this data via ``self.branch_map[self.branch]``, or via ``self.branch_data`` by convenience https://github.com/riga/law/blob/master/examples/htcondor_at_cern/analysis/tasks.py
        2. where does output_{}.json save at?
        3. .req is calling the function
        4. what is --local-scheduler, --background
- problem
    - How to access task visualizer
        - error running ```luigid ``` https://github.com/riga/law/tree/master/examples/htcondor_at_cern#4-run-the-createalphabet-task
        ```
        $ luigid
        Traceback (most recent call last):
          File "/afs/cern.ch/user/m/mrieger/public/law_sw/luigi_3/bin/luigid", line 5, in <module>
            import luigi.cmdline
          File "/afs/cern.ch/user/m/mrieger/public/law_sw/luigi_3/luigi/__init__.py", line 23, in <module>
            from luigi import task
          File "/afs/cern.ch/user/m/mrieger/public/law_sw/luigi_3/luigi/task.py", line 147
            class Task(metaclass=Register):
                                ^
        SyntaxError: invalid syntax
        ```
    - How to activate SNDSW
    - How to access EOS
    
    - how to activate a new class
    - how to access GPU cluster
    - how to activate conda env on HTCondor


        
    
- TODO:
    1. use SNDSW particle gun to generte MC events (different pid differnt task)
    2. convert MC data into digitized data
    3. extract hits coordinates from digitized data
    4. generated ML input data
    5. traning
    6. ploting results
